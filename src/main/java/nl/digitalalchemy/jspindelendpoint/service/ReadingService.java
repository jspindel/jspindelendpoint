package nl.digitalalchemy.jspindelendpoint.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.digitalalchemy.jspindelendpoint.dao.ReadingRepository;
import nl.digitalalchemy.jspindelendpoint.dto.IspindelPostMessage;
import nl.digitalalchemy.jspindelendpoint.entity.ReadingEntity;

@Service
public class ReadingService {

	@Autowired
	ReadingRepository readingRepository;
	
	public boolean storeReadingReturnSuccess(IspindelPostMessage postMessage) {
		ReadingEntity entity = readingRepository.save(new ReadingEntity(postMessage.getName(),LocalDateTime.now(),new BigDecimal(postMessage.getAngle()),new BigDecimal(postMessage.getTemperature()),new BigDecimal(postMessage.getBattery()),new BigDecimal(postMessage.getGravity())));
		return true;
	}
}
