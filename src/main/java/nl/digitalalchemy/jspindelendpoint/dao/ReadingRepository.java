package nl.digitalalchemy.jspindelendpoint.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.digitalalchemy.jspindelendpoint.entity.ReadingEntity;

@Repository
public interface ReadingRepository extends CrudRepository<ReadingEntity, Long> {
	
}

