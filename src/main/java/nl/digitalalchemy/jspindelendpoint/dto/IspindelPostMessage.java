package nl.digitalalchemy.jspindelendpoint.dto;

public class IspindelPostMessage {

	/*
	 * IspindelPostMessage [name=iSpindel000, token=BBFF-r6liHOc3nhnWjpSLLHfZi7OURjrQrj, angle=41.15717, temperature=23.1875, temp_units=C, battery=4.171011, gravity=8.34543, interval=10, RSSI=null]
	 */

	
	private String name;
	private String token;
	private String angle;
	private String temperature;
	private String temp_units;
	private String battery;
	private String gravity;
	private String interval;
	private String RSSI;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAngle() {
		return angle;
	}
	public void setAngle(String angle) {
		this.angle = angle;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getTemp_units() {
		return temp_units;
	}
	public void setTemp_units(String temp_units) {
		this.temp_units = temp_units;
	}
	public String getBattery() {
		return battery;
	}
	public void setBattery(String battery) {
		this.battery = battery;
	}
	public String getGravity() {
		return gravity;
	}
	public void setGravity(String gravity) {
		this.gravity = gravity;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getRSSI() {
		return RSSI;
	}
	public void setRSSI(String rSSI) {
		RSSI = rSSI;
	}
	@Override
	public String toString() {
		return "IspindelPostMessage [name=" + name + ", token=" + token + ", angle=" + angle + ", temperature="
				+ temperature + ", temp_units=" + temp_units + ", battery=" + battery + ", gravity=" + gravity
				+ ", interval=" + interval + ", RSSI=" + RSSI + "]";
	}
	
}
