package nl.digitalalchemy.jspindelendpoint.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "reading")
public class ReadingEntity implements Serializable {
	
	private static final long serialVersionUID = -6814454219034597810L;

	public ReadingEntity(String spindel, LocalDateTime dateTime, BigDecimal angle, BigDecimal temperature,
			BigDecimal battery, BigDecimal gravity) {
		super();
		this.spindel = spindel;
		this.dateTime = dateTime;
		this.angle = angle;
		this.temperature = temperature;
		this.battery = battery;
		this.gravity = gravity;
	}
	
	public ReadingEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false,name = "spindel")
	private String spindel;
	
	@Column(nullable = false,name = "datetime")
	private LocalDateTime dateTime;
	
	@Column(nullable = false)
	private BigDecimal angle;
	
	@Column(nullable = false)
	private BigDecimal temperature;
	
	@Column(nullable = false)
	private BigDecimal battery;
	
	@Column(nullable = false)
	private BigDecimal gravity;
	

	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	public BigDecimal getAngle() {
		return angle;
	}
	public void setAngle(BigDecimal angle) {
		this.angle = angle;
	}
	public BigDecimal getTemperature() {
		return temperature;
	}
	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}
	public BigDecimal getBattery() {
		return battery;
	}
	public void setBattery(BigDecimal battery) {
		this.battery = battery;
	}
	public BigDecimal getGravity() {
		return gravity;
	}
	public void setGravity(BigDecimal gravity) {
		this.gravity = gravity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	public String getSpindel() {
		return spindel;
	}

	public void setSpindel(String spindel) {
		this.spindel = spindel;
	}

	@Override
	public String toString() {
		return "Reading [time= "+dateTime +" name=" + spindel + ", angle=" + angle + ", temperature="
				+ temperature + ", battery=" + battery + ", gravity=" + gravity+" ]";
	}	
	

}
