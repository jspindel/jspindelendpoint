package nl.digitalalchemy.jspindelendpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JspindelendpointApplication {

	public static void main(String[] args) {
		SpringApplication.run(JspindelendpointApplication.class, args);
	}

}
